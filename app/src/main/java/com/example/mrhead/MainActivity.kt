package com.example.mrhead

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView


class MainActivity : AppCompatActivity() {

    // Deklarasi Global variabel
    private lateinit var rambut: ImageView
    private lateinit var alis: ImageView
    private lateinit var kumis: ImageView
    private lateinit var jenggot: ImageView
    private lateinit var cekRambut: CheckBox
    private lateinit var cekAlis: CheckBox
    private lateinit var cekKumis: CheckBox
    private lateinit var cekJenggot: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Inisialisasi Variabel
        rambut = findViewById(R.id.hair)
        alis = findViewById(R.id.eyebrow)
        kumis = findViewById(R.id.moustache)
        jenggot = findViewById(R.id.beard)
        cekRambut = findViewById(R.id.check1)
        cekAlis = findViewById(R.id.check2)
        cekKumis = findViewById(R.id.check3)
        cekJenggot = findViewById(R.id.check4)

        // Rambut
        cekRambut.setOnClickListener {
            rambut.visibility = if (cekRambut.isChecked) View.VISIBLE else View.GONE
        }

        // Alis
        cekAlis.setOnClickListener {
            alis.visibility = if (cekAlis.isChecked) View.VISIBLE else View.GONE
        }

        // Kumis
        cekKumis.setOnClickListener {
            kumis.visibility = if (cekKumis.isChecked) View.VISIBLE else View.GONE
        }

        // Jenggot
        cekJenggot.setOnClickListener {
            jenggot.visibility = if (cekJenggot.isChecked) View.VISIBLE else View.GONE
        }
    }
}